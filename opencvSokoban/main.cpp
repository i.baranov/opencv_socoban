#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <conio.h>
#include <fstream>
#include <vector>
#include <string>

using namespace std;
using namespace cv;

const int field_size = 64;
const int width = field_size * 10;
const int height = field_size * 10;
int snakeLength = 2;

struct Tile {
	int x;
	int y;
};

enum GameObject {
	HUMAN,
	BOX,
	WALL,
	FLOOR,
	HUMANT, 
	BOXT,
	FLOORT
};

using namespace std;
using namespace cv;

void drawHuman(Tile position, cv::Mat world)
{
	Mat m = Mat(64, 64, CV_8UC3);

	for (int x = 0; x < 64; x++)
	{
		for (int y = 0; y < 64; y++)
		{
			m.at<Vec3b>(x, y) = Vec3b(255, 255, 255);
		}
	}


	circle(m, { 30, 16 }, 10, Scalar(0, 0, 0));

	line(m, { 30,27 }, { 30,47 }, Scalar(0, 0, 0));
	line(m, { 30,27 }, { 22,35 }, Scalar(0, 0, 0));
	line(m, { 30,27 }, { 38,35 }, Scalar(0, 0, 0));
	line(m, { 30,47 }, { 22,55 }, Scalar(0, 0, 0));
	line(m, { 30,47 }, { 38,55 }, Scalar(0, 0, 0));

	imwrite("human.png", m);

	m.copyTo(world(Rect(position.x * field_size, position.y * field_size, m.cols, m.rows)));
}

void drawHumanWithTarget(Tile position, cv::Mat world)
{
	Mat m = Mat(64, 64, CV_8UC3);

	for (int x = 0; x < 64; x++)
	{
		for (int y = 0; y < 64; y++)
		{
			m.at<Vec3b>(x, y) = Vec3b(255, 255, 255);
		}
	}


	circle(m, { 30, 16 }, 10, Scalar(0, 0, 0));

	line(m, { 30,27 }, { 30,47 }, Scalar(0, 0, 0));
	line(m, { 30,27 }, { 22,35 }, Scalar(0, 0, 0));
	line(m, { 30,27 }, { 38,35 }, Scalar(0, 0, 0));
	line(m, { 30,47 }, { 22,55 }, Scalar(0, 0, 0));
	line(m, { 30,47 }, { 38,55 }, Scalar(0, 0, 0));

	circle(m, { 32, 32 }, 30, Scalar(29, 230, 181));

	imwrite("human.png", m);

	m.copyTo(world(Rect(position.x * field_size, position.y * field_size, m.cols, m.rows)));
}

void drawWall(Tile position, cv::Mat world)
{
	Mat m = Mat(64, 64, CV_8UC3);

	m = Scalar(255, 255, 255);

	for (int n = 0; n < 4; n++)
	{
		for (int i = 0; i < 4; i++)
		{
			rectangle(m, { 0 + 16 * i,0 + n * 16 }, { 15 + i * 16, 7 + n * 16 }, Scalar(21, 0, 136));
			rectangle(m, { 2 + 16 * i,1 + n * 16 }, { 14 + i * 16, 5 + n * 16 }, Scalar(87, 122, 185), FILLED);
			line(m, { 1 + 16 * i, 1 + n * 16 }, { 1 + 16 * i,6 + n * 16 }, Scalar(52, 78, 124));
			line(m, { 1 + 16 * i,6 + n * 16 }, { 14 + i * 16,6 + n * 16 }, Scalar(52, 78, 124));
		}
	}

	for (int n = 0; n < 5; n++)
	{
		for (int i = 0; i < 5; i++)
		{
			rectangle(m, { -8 + 16 * i,8 + n * 16 }, { 7 + i * 16, 15 + n * 16 }, Scalar(21, 0, 136));
			rectangle(m, { -6 + 16 * i,9 + n * 16 }, { 6 + i * 16, 13 + n * 16 }, Scalar(87, 122, 185), FILLED);
			line(m, { -7 + 16 * i, -7 + n * 16 }, { -7 + 16 * i, -2 + n * 16 }, Scalar(52, 78, 124));
			line(m, { -7 + 16 * i, -2 + n * 16 }, { 6 + i * 16,-2 + n * 16 }, Scalar(52, 78, 124));
		}
	}

	imwrite("wall.png", m);

	m.copyTo(world(Rect(position.x * field_size, position.y * field_size, m.cols, m.rows)));
}

void drawBox(Tile position, cv::Mat world)
{
	Mat m = Mat(64, 64, CV_8UC3);

	m = Scalar(255, 255, 255);
	int stop;

	for (int x = 0; x < 64; x++)
	{
		for (int y = 0; y < 64; y++)
		{
			m.at<Vec3b>(x, y) = Vec3b(255, 255, 255);
		}
	}

	for (int i = 0; i < 31; i++)
	{
		line(m, { 33,31 + i }, { 59,18 + i }, Scalar(106, 143, 194));
		line(m, { 6,18 + i }, { 31,30 + i }, Scalar(81, 109, 148));
	}
	for (int i = 0; i < 26; i++)
	{
		if (i < 14)
		{
			line(m, { 32 - 2 * i, 4 + i }, { 33 + 2 * i,4 + i }, Scalar(90, 166, 189));
			stop = i;
		}
		else {
			line(m, { 32 - 2 * stop + 2 * (i - stop), 4 + i }, { 33 + 2 * stop - 2 * (i - stop),4 + i }, Scalar(90, 166, 189));
		}
	}

	line(m, { 32,30 }, { 6,17 }, Scalar(134, 185, 213));
	line(m, { 32,31 }, { 59,17 }, Scalar(134, 185, 213));
	line(m, { 32,30 }, { 32,61 }, Scalar(134, 185, 213));

	line(m, { 33,3 }, { 59,16 }, Scalar(70, 90, 117));
	line(m, { 32,3 }, { 6,16 }, Scalar(70, 90, 117));
	line(m, { 59,49 }, { 32,62 }, Scalar(70, 90, 117));
	line(m, { 6,48 }, { 32,62 }, Scalar(70, 90, 117));
	line(m, { 5,17 }, { 5,48 }, Scalar(70, 90, 117));
	line(m, { 33,4 }, { 58,16 }, Scalar(90, 166, 189));

	line(m, { 46,9 }, { 20,22 }, Scalar(229, 219, 212));
	line(m, { 21,9 }, { 46,23 }, Scalar(229, 219, 212));
	line(m, { 19,23 }, { 19,54 }, Scalar(183, 178, 175));
	line(m, { 46,24 }, { 46,56 }, Scalar(239, 233, 228));

	imwrite("box.png", m);

	m.copyTo(world(Rect(position.x * field_size, position.y * field_size, m.cols, m.rows)));
}

void drawBoxWithTarget(Tile position, cv::Mat world)
{
	Mat m = Mat(64, 64, CV_8UC3);

	m = Scalar(255, 255, 255);
	int stop;

	for (int x = 0; x < 64; x++)
	{
		for (int y = 0; y < 64; y++)
		{
			m.at<Vec3b>(x, y) = Vec3b(255, 255, 255);
		}
	}

	for (int i = 0; i < 31; i++)
	{
		line(m, { 33,31 + i }, { 59,18 + i }, Scalar(106, 143, 194));
		line(m, { 6,18 + i }, { 31,30 + i }, Scalar(81, 109, 148));
	}
	for (int i = 0; i < 26; i++)
	{
		if (i < 14)
		{
			line(m, { 32 - 2 * i, 4 + i }, { 33 + 2 * i,4 + i }, Scalar(90, 166, 189));
			stop = i;
		}
		else {
			line(m, { 32 - 2 * stop + 2 * (i - stop), 4 + i }, { 33 + 2 * stop - 2 * (i - stop),4 + i }, Scalar(90, 166, 189));
		}
	}

	line(m, { 32,30 }, { 6,17 }, Scalar(134, 185, 213));
	line(m, { 32,31 }, { 59,17 }, Scalar(134, 185, 213));
	line(m, { 32,30 }, { 32,61 }, Scalar(134, 185, 213));

	line(m, { 33,3 }, { 59,16 }, Scalar(70, 90, 117));
	line(m, { 32,3 }, { 6,16 }, Scalar(70, 90, 117));
	line(m, { 59,49 }, { 32,62 }, Scalar(70, 90, 117));
	line(m, { 6,48 }, { 32,62 }, Scalar(70, 90, 117));
	line(m, { 5,17 }, { 5,48 }, Scalar(70, 90, 117));
	line(m, { 33,4 }, { 58,16 }, Scalar(90, 166, 189));

	line(m, { 46,9 }, { 20,22 }, Scalar(229, 219, 212));
	line(m, { 21,9 }, { 46,23 }, Scalar(229, 219, 212));
	line(m, { 19,23 }, { 19,54 }, Scalar(183, 178, 175));
	line(m, { 46,24 }, { 46,56 }, Scalar(239, 233, 228));

	imwrite("box.png", m);
	circle(m, { 32, 32 }, 30, Scalar(29, 230, 181));

	m.copyTo(world(Rect(position.x * field_size, position.y * field_size, m.cols, m.rows)));
}

void drawFloor(Tile position, cv::Mat world)
{
	Mat m = Mat(64, 64, CV_8UC3);

	m = Scalar(255, 255, 255);

	m.copyTo(world(Rect(position.x * field_size, position.y * field_size, m.cols, m.rows)));
}

void drawFloorWithTarget(Tile position, cv::Mat world)
{
	Mat m = Mat(64, 64, CV_8UC3);

	m = Scalar(255, 255, 255);

	circle(m, { 32, 32 }, 30, Scalar(29, 230, 181));

	m.copyTo(world(Rect(position.x * field_size, position.y * field_size, m.cols, m.rows)));
}

void drawLadder(Mat world, Tile position)
{
	Mat m = Mat(64, 64, CV_8UC3);

	m = Scalar(0,0,0);
	int stop;

	for (int i = 0; i < 5; i++)
	{
		if (i < 5) rectangle(m, { 4,56 - i * 16 }, { 59,59 - i * 16 }, Scalar(41, 81, 105), FILLED);
		rectangle(m, { 8,60 - i * 16 }, { 15,67 - i * 16 }, Scalar(41, 81, 105), FILLED);
		rectangle(m, { 48,60 - i * 16 }, { 55,67 - i * 16 }, Scalar(41, 81, 105), FILLED);
		if (i < 5) rectangle(m, { 4,52 - i * 16 }, { 59,55 - i * 16 }, Scalar(82, 136, 172), FILLED);
	}

	m.copyTo(world(Rect(position.x * field_size, position.y * field_size, m.cols, m.rows)));
}

class GameWorld {
public:
	cv::Mat m;
	Tile player;
	std::vector<std::vector<GameObject>> objects;
	int currentLevel;

	GameWorld() {
		m = cv::Mat(width, height, CV_8UC3);
		currentLevel = 1;
	}

	void draw()
	{
		m = cv::Scalar(255, 255, 255);
		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				switch (objects[i][j])
				{
				case FLOOR: drawFloor({ i,j }, m); break;
				case FLOORT: drawFloorWithTarget({ i,j }, m); break;
				case HUMAN: drawHuman({ i,j }, m); break;
				case HUMANT: drawHumanWithTarget({ i,j }, m); break;
				case BOX:  drawBox({ i,j }, m); break;
				case BOXT: drawBoxWithTarget({ i,j }, m); break;
				case WALL: drawWall({ i,j }, m); break;
				}
			}
		}

		cv::imshow("Socoban", m);
	}

	void resetState()
	{
		std::string cur = std::to_string(currentLevel);
		loadWorld("level" + cur + ".skb");
	}

	void loadWorld(std::string filename) {
		
		ifstream fin(filename);

		if (!fin.is_open())
		{
			cout << "File " << filename << " cannot be open" << endl;
		}
		else
		{
			char buffer[100];
			int k = 0;

			while (fin) {
				char c = fin.get();
				if (c == '\n') continue;
				buffer[k] = c;
				k++;
			}

			for (int i = 0; i < 10; i++)
			{
				for (int j = 0; j < 10; j++)
				{
					objects[i][j] = static_cast<GameObject>(buffer[i*10+j]-'0');
					if (objects[i][j] == HUMAN) player = { j,i };
				}
			}
		}
	}

	void saveWorld(std::string filename)
	{
		std::ofstream fout(filename);

		for (int i = 0; i < 10; i++)
		{
			if (i != 0) fout << endl;
			for (int j = 0; j < 10; j++)
			{
				fout << objects[i][j];
			}
		}
	}

	void changeLevel()
	{
		if (currentLevel <= 3) resetState();
	}

	void playerMove(int dx, int dy)
	{
		if (objects[player.y + dy][player.x + dx] == FLOOR)
		{
			objects[player.y + dy][player.x + dx] = HUMAN;
			if (objects[player.y][player.x] == HUMAN) objects[player.y][player.x] = FLOOR;
			if (objects[player.y][player.x] == HUMANT) objects[player.y][player.x] = FLOORT;
			player = { player.x + dx, player.y + dy };
			return;
		}

		if (objects[player.y + dy][player.x + dx] == FLOORT)
		{
			objects[player.y + dy][player.x + dx] = HUMANT;
			if (objects[player.y][player.x] == HUMAN) objects[player.y][player.x] = FLOOR;
			if (objects[player.y][player.x] == HUMANT) objects[player.y][player.x] = FLOORT;
			player = { player.x + dx, player.y + dy };
			return;
		}
		
		if (objects[player.y + dy][player.x + dx] == BOX || objects[player.y + dy][player.x + dx] == BOXT)
		{
			if (objects[player.y + 2 * dy][player.x + 2 * dx] == FLOOR) {
				objects[player.y + 2 * dy][player.x + 2 * dx] = BOX;
				if (objects[player.y + dy][player.x + dx] == BOXT) { objects[player.y + dy][player.x + dx] = HUMANT; }
				else { objects[player.y + dy][player.x + dx] = HUMAN; }
				if (objects[player.y][player.x] == HUMAN) objects[player.y][player.x] = FLOOR;
				if (objects[player.y][player.x] == HUMANT) objects[player.y][player.x] = FLOORT;
				player = { player.x + dx, player.y + dy };
			}
			else if (objects[player.y + 2 * dy][player.x + 2 * dx] == FLOORT) {
				objects[player.y + 2 * dy][player.x + 2 * dx] = BOXT;
				if (objects[player.y + dy][player.x + dx] == BOX) objects[player.y + dy][player.x + dx] = HUMAN;
				else objects[player.y + dy][player.x + dx] = HUMANT;
				if (objects[player.y][player.x] == HUMAN) objects[player.y][player.x] = FLOOR;
				if (objects[player.y][player.x] == HUMANT) objects[player.y][player.x] = FLOORT;
				player = { player.x + dx, player.y + dy };
			}

			bool out = false;
			for (int i = 0; i < 10; i++)
			{
				for (int j = 0; j < 10; j++)
				{
					if (objects[i][j] == BOX) out = true;
				}
				if (out) break;
			}
			if (!out) {
				currentLevel++;
				changeLevel();
			}

			return;
		}
	}

	void gameLoop()
	{
		char ch;
		int key;
		while (true)
		{
			m = cv::Scalar(255, 255, 255);

			ch = cv::waitKey(100);
			key = static_cast<int>(ch);

			switch (key)
			{
			case 'w': playerMove(-1, 0); break;
			case 's': playerMove(1, 0); break;
			case 'a': playerMove(0, -1); break;
			case 'd': playerMove(0, 1); break;
			case 'p': resetState();
			}
		
			draw();

			if (currentLevel > 3)
			{
				m = Scalar(0, 0, 0);
				putText(m, "Victory!", { 200,200 }, FONT_HERSHEY_SIMPLEX, 2, Scalar(255, 255, 255), 2);
				putText(m, "press any key to exit", { 120,300 }, FONT_HERSHEY_SIMPLEX, 1.2, Scalar(255, 255, 255), 2);
				for (int i=0; i<4; i++)
					drawLadder(m, { 3 + i,6 });
				imshow("Socoban", m);
				waitKey();
				return;
			}
		}
	}
};

void createLevels(GameWorld* gw)
{
	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			gw->objects[i][j] = WALL;
		}
	}

	for (int i = 0; i < 6; i++)
	{
		if (i < 4)gw->objects[3 + i][1] = FLOOR;
		if (i < 2) gw->objects[4 + i][2] = FLOOR;
		gw->objects[2 + i][3] = FLOOR;
		gw->objects[2 + i][4] = FLOOR;
		gw->objects[2 + i][5] = FLOOR;
		gw->objects[2 + i][6] = FLOOR;
	}
	gw->objects[4][1] = HUMAN;
	gw->objects[3][1] = FLOORT;
	gw->objects[6][1] = FLOORT;
	gw->objects[2][6] = FLOORT;
	gw->objects[6][6] = FLOORT;
	gw->objects[4][2] = BOX;
	gw->objects[4][5] = BOX;
	gw->objects[6][3] = BOX;
	gw->objects[3][3] = BOX;
	gw->objects[5][6] = WALL;
	gw->objects[5][5] = WALL;

	gw->saveWorld("level1.skb");

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			gw->objects[i][j] = WALL;
		}
	}
	gw->objects[1][2] = WALL;
	gw->objects[1][3] = WALL;
	gw->objects[1][1] = FLOORT;
	gw->objects[2][1] = FLOORT;
	gw->objects[3][1] = FLOORT;
	gw->objects[2][2] = BOX;
	gw->objects[2][4] = BOX;
	gw->objects[3][4] = BOX;
	gw->objects[2][3] = HUMAN;
	gw->objects[3][3] = FLOOR;
	gw->objects[3][2] = FLOOR;
	gw->objects[4][4] = FLOOR;
	gw->objects[1][4] = FLOOR;
	gw->objects[1][5] = FLOOR;
	gw->objects[1][6] = FLOOR;
	gw->objects[2][5] = FLOOR;
	gw->objects[2][6] = FLOOR;
	gw->objects[3][5] = FLOOR;

	gw->saveWorld("level2.skb");

	for (int i = 0; i < 10; i++)
	{
		for (int j = 0; j < 10; j++)
		{
			gw->objects[i][j] = WALL;
		}
	}
	for (int i = 1; i < 3; i++)
	{
		for (int j = 1; j < 8; j++)
		{
			gw->objects[i][j] = FLOOR;
		}
	}
	for (int i = 3; i < 6; i++)
	{
		for (int j = 2; j < 7; j++)
		{
			gw->objects[i][j] = FLOOR;
		}
	}
	for (int i = 6; i < 9; i++)
	{
		for (int j = 3; j < 6; j++)
		{
			gw->objects[i][j] = FLOORT;
		}
	}

	gw->objects[3][3] = WALL;
	gw->objects[8][3] = WALL;
	gw->objects[3][5] = WALL;
	gw->objects[6][3] = FLOOR;
	gw->objects[1][7] = HUMAN;
	gw->objects[4][3] = BOX;
	gw->objects[3][2] = BOX;
	gw->objects[2][3] = BOX;
	gw->objects[3][4] = BOX;
	gw->objects[2][5] = BOX;
	gw->objects[3][6] = BOX;
	gw->objects[4][5] = BOX;

	gw->saveWorld("level3.skb");
}

int main() {
	GameWorld *gw = new GameWorld();

	gw->objects.resize(10);
	for (auto& v : gw->objects) {
		v.resize(10);
	}
    
	createLevels(gw);

	gw->loadWorld("level1.skb");

	gw->gameLoop();

	return 0;
}